import { Injectable } from "./di/decorators";

@Injectable()
export class A {
    speak(): string {
        return 'A';
    }
}

@Injectable()
export class B {
    speak(): string {
        return 'B';
    }
}

@Injectable()
export class Test1 {
    
    constructor(private a: A, private b: B) {

    }

    
    speak(): string {
        return `${this.a.speak()}.${this.b.speak()}`;
    }
}
