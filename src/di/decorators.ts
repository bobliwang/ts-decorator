import 'reflect-metadata'

export interface ProviderMeta {
    ctor: any;
    paramTypes: any [];
}

export class Injector {

    public static readonly Instance = new Injector();

    public providers: ProviderMeta[] = [];

    public resolve<T>(type: { new(...args): T }): T {
        const provider = this.providers.find(p => p.ctor === type);

        const paramValues = provider.paramTypes.map(paramType => this.resolve(paramType));

        const result = new provider.ctor(...paramValues);

        return result;
    }
}

export const Injectable = (): ClassDecorator => {
    return target => {
        const paramTypes = Reflect.getMetadata('design:paramtypes', target) || [];

        Injector.Instance.providers.push( { ctor: target, paramTypes });
        
    };
};

