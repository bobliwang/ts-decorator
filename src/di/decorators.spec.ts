import { expect } from 'chai';
import { Injector } from './decorators';
import { Test1, A, B } from '../models';

describe('decorators tests', () => {

    it('Injector.Instance.providers', async () => {
        
        expect(Injector.Instance.providers).to.not.null;

        expect(Injector.Instance.providers.length).to.equal(3);

        expect(Injector.Instance.providers[0].ctor).to.equal(A);
        expect(Injector.Instance.providers[1].ctor).to.equal(B);
        expect(Injector.Instance.providers[2].ctor).to.equal(Test1);

    });

    it('Injector resolve', async () => {
        
        const test1 = Injector.Instance.resolve(Test1);

        expect(test1.speak()).to.equal('A.B');
        
    });

});
